- In 2018 Engineering Productivity was a new team formed by combining 3 teams: 
	
	1. Quality and Test Engineering (QTE),
	2. Release Engineering
	3. Web Performance

- We lacked confidence because we didn't have the data to prove many of our assumptions.
- We wanted to use data to inform our decision-making instead of practicing educated guesswork.